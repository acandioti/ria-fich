$(document).ajaxStart(function(){
    $("html").addClass("wait");
});
$(document).ajaxStop(function(){
    $("html").removeClass("wait");
});
$(document).ajaxError(function(){
    $("html").removeClass("wait");
});
$(document).ready(function() {
    $('input.identificador').on('input',function(){
        this.value=this.value.replace(/[^0-9/]/g,'');
    });

    /*$('button[type=submit]:not([value=Reporte]),input[type=submit]:not([value=Reporte])').on("click",function () {
        $("html").addClass("wait");
    });*/

    $(".number-control").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('.btn-reset').on('click',function(){
        var formulario = $(this).parents('form');
        try{formulario.find('input').val('');}catch(e){};
        try{formulario.find('select').val('').trigger('change');}catch(e){};
        try{formulario.find('textarea').innerHTML('');}catch(e){};
        formulario.submit();
    });

});

function boxrefresh(box){
    $(box).append("<div class=\"overlay\"><i class=\"fa fa-refresh fa-spin\"></i></div>");
}
function boxstop(box){
    $(box).find('div.overlay').remove();
}

function goToElement(id){
    $('html,body').animate({
            scrollTop: $(id).offset().top},
        'slow');
}

function alerta(mensaje, tipo, before){

    var type='success';
    if (tipo!=undefined){
        if (tipo == 1 || tipo == 'success') type='success';
        if (tipo == 2 || tipo == 'warning') type='warning';
        if (tipo == 3 || tipo == 'danger') type='danger';
    }

    var html = '<div class="alert callout callout-'+type+' callout-dismissible" role="alert">'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
        mensaje+
        '</div>';

    if (before){
        if ($(before).prev().hasClass('alert')){
            $(before).prev().remove();
        }
        $(before).before(html);
        goToElement($(before).prev());
    }else {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#mensajes").html(html);
    }

    $(".alert").hide();
    $(".alert").fadeIn('slow');
}
