<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coleccion
 *
 * @ORM\Table(name="coleccion")
 * @ORM\Entity
 */
class Coleccion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="snrd", type="string", length=100, nullable=false)
     */
    private $snrd;

    /**
     * @var string
     *
     * @ORM\Column(name="openaire", type="string", length=100, nullable=false)
     */
    private $openaire;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Version", inversedBy="Coleccion")
     * @ORM\JoinTable(name="coleccion__version",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_coleccion", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_version", referencedColumnName="id")
     *   }
     * )
     */
    private $cVersion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cVersion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getSnrd()
    {
        return $this->snrd;
    }

    /**
     * @param string $snrd
     */
    public function setSnrd($snrd)
    {
        $this->snrd = $snrd;
    }

    /**
     * @return string
     */
    public function getOpenaire()
    {
        return $this->openaire;
    }

    /**
     * @param string $openaire
     */
    public function setOpenaire($openaire)
    {
        $this->openaire = $openaire;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCVersion()
    {
        return $this->cVersion;
    }



}

