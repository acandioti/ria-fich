<?php

namespace App\Exceptions;

class DSpaceException extends \Exception
{

    public $tipo;
    public $exception;

    /**
     * SinmpaException constructor.
     * @param $tipo
     */
    public function __construct($mensaje,$tipo='danger',\Exception $exception = null)
    {
        parent::__construct($mensaje);
        $this->tipo = $tipo;
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @return \Exception
     */
    public function getException()
    {
        return $this->exception;
    }


}