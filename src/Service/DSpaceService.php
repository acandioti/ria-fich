<?php
namespace App\Service;

use App\Service\Helper;
use App\Entity\Configuracion;
use App\Exceptions\DSpaceException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class DSpaceService
{
    private $servidor = "";
    private $puerto = "";
    private $usuario = "";
    private $password = "";
    private $cookie;
    private $client;
    private $conectado = false;
    private $helper;
    private $entityManager;
    private $url = '';

    public function __construct(
        EntityManagerInterface $entityManager,
        Helper $helper,
        $dominio,
        $puerto,
        $usuario,
        $clave,
        $url
    )
    {
        $this->servidor = $dominio;
        $this->puerto = $puerto;
        $this->usuario = $usuario;
        $this->password = $clave;
        $this->url = $url;
        $this->client = new Client();
        $this->helper = $helper;
        $this->entityManager = $entityManager;
    }

    private function conectar()
    {
        if($this->conectado){
            $this->helper->info('conectado');
            return true;
        }

        try
        {
            $url = "http://".$this->servidor.":".$this->puerto."/rest/login";
            $this->helper->info('conectando: ' . $url);
            $data = ["email" => $this->usuario,"password" => $this->password];
            $response = $this->client->post($url, ["query" => $data]);
            //$result = json_decode($response->getBody()->getContents());
            if($response->getStatusCode() == 200 ){
                $this->helper->info('conectado: ');
                $this->conectado = true;
                $this->cookie = $response->getHeader('Set-Cookie')[0];
            } else {
                $this->helper->info('No conectado: ');
                throw new DSpaceException($response->getStatusCode() . "::" . $response->getReasonPhrase());
            }
        }
        catch (RequestException $e)
        {
            $this->helper->error($e);
            $this->conectado = false;
            throw new DSpaceException($e->getMessage());
        }
    }

    private function status()
    {
        try
        {
            $response = $this->client->get($this->getUrl('status'),
                [
                    "headers" => array(
                        'cookie' => $this->cookie,
                        'Accept' => 'application/json',
                    )
                ]);
        }
        catch (RequestException $e)
        {
            $this->helper->info($e);
        }
        return $this->conectado;
    }

    private function get($endpoint, $prefix = 'rest')
    {
        $this->conectar();

        try {
            $response = $this->client->get($this->getUrl($endpoint,$prefix),
                [
                    "headers" => array(
                        'cookie' => $this->cookie,
                        'Accept' => 'application/json',
                    )
                ]);
            if($response->getStatusCode() == 200){
                $this->helper->info($response->getBody());
                return json_decode($response->getBody(), true);
            } else {
                throw new DSpaceException($response->getStatusCode() . "::" . $response->getReasonPhrase());
            }
        } catch (RequestException $e) {
            $this->helper->error($e);
            $this->conectado = false;
            throw new DSpaceException($e->getMessage());
        }
    }

    private function post($endpoint, $data)
    {
        $this->conectar();

        try {
            $response = $this->client->post($this->getUrl($endpoint),
                [
                    "json" => $data,
                    "headers" => [
                        'cookie' => $this->cookie,
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        ]
                ]
            );
            if($response->getStatusCode() == 200){
                $this->helper->info($response->getBody());
                return json_decode($response->getBody(), true);
            } else {
                throw new DSpaceException($response->getStatusCode() . "::" . $response->getReasonPhrase());
            }

        } catch (RequestException $e) {
            $this->helper->error($e);
            $this->conectado = false;
            throw new DSpaceException($e->getMessage());
        }
    }

    private function delete($endpoint)
    {
        $this->conectar();

        try {
            $response = $this->client->delete($this->getUrl($endpoint),
                [
                    "json" => [],
                    "headers" => [
                        'cookie' => $this->cookie,
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        ]
                ]
            );

            if($response->getStatusCode() == 200){
                $this->helper->info($response->getBody());
                return json_decode($response->getBody(), true);
            } else {
                throw new DSpaceException($response->getStatusCode() . "::" . $response->getReasonPhrase());
            }

        } catch (RequestException $e) {
            $e->getMessage();
            $this->helper->error($e);
            $this->conectado = false;
            //throw new DSpaceException($e->getMessage());
        }
    }

    private function put($endpoint, $data)
    {
        $this->conectar();

        try {
            $response = $this->client->put($this->getUrl($endpoint),
                [
                    "json" => $data,
                    "headers" => [
                        'cookie' => $this->cookie,
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        ]
                ]
            );

            if($response->getStatusCode() == 200){
                $this->helper->info($response->getBody());
                return json_decode($response->getBody(), true);
            } else {
                throw new DSpaceException($response->getStatusCode() . "::" . $response->getReasonPhrase());
            }

        } catch (RequestException $e) {
            $this->helper->error($e);
            $this->conectado = false;
            throw new DSpaceException($e->getMessage());
        }
    }

    private function postFile($idItem, $path, $nombre, $description="")
    {
        $this->conectar();

        try {

            $resource = fopen($path, 'r');
            $response = $this->client->post($this->getUrl("items/$idItem/bitstreams?description=$description&name=".$nombre),
                [
                    "body" => $resource,
                    "headers" => [
                        'cookie' => $this->cookie,
                        'Accept' => 'application/json',
                        'Content-Type' => 'multipart/form-data',
                    ]
                ]
            );
            if($response->getStatusCode() == 200){
                return json_decode($response->getBody()->getContents(), true);
            } else {
                $this->helper->info($response->getStatusCode() . "::" . $response->getReasonPhrase());
                throw new DSpaceException("Error al subir el archivo.");
            }

        } catch (RequestException $e) {
            $this->helper->error($e);
            $this->conectado = false;
            throw new DSpaceException("Error al subir el archivo.");
        }
    }

    /*
     * Archivo
     */

    public function getFile($id)
    {
        $this->conectar();

        try {
            $response = $this->client->get($this->getUrl("bitstreams/$id/retrieve"),
                [
                    "headers" => [
                        'cookie' => $this->cookie,
                    ],
                    'sink' => '/tmp/'.$id
                ]
            );

            if($response->getStatusCode() == 200){
                $this->helper->info(json_encode($response->getHeaders()));
                return $response->getHeaders();
            } else {
                throw new DSpaceException($response->getStatusCode() . "::" . $response->getReasonPhrase());
            }

        } catch (RequestException $e) {
            $this->helper->error($e);
            $this->conectado = false;
            throw new DSpaceException($e->getMessage());
        }
    }

    public function archivoEliminar($id){
        return $this->delete("bitstreams/".$id);
    }

    /*
     * Item
     */

    public function itemGuardar($item, $archivos = []){
        if (isset($item['parentCollection']['id'])){
            $idColeccion = $item['parentCollection']['id'];
        }else{
            $idColeccion = $item['parentCollection']['uuid'];
        }
        unset($item['parentCollection']);

        for($i = 0; $i< count($item['metadata']); $i++){
            unset($item['metadata'][$i]['schema']);
            unset($item['metadata'][$i]['element']);
            unset($item['metadata'][$i]['qualifier']);
        }

        $uuid = null;
        if (isset($item['uuid']) && $item['uuid']!=null) {
            $uuid = $item['uuid'];
        }

        if ($uuid){
            $response = $this->put("items/{$uuid}/metadata?expand=all", $item['metadata']);
        }else{
            $response = $this->post("collections/$idColeccion/items?expand=all", $item);
            $uuid = $response['uuid'];
        }
        foreach ($archivos as $archivo){
            $this->postFile($uuid, $archivo['ruta'], $archivo['nombre'],$archivo['descripcion']);
        }

        return $this->getItem($response['uuid']);
    }

    public function itemEliminar($id){
        return $this->delete("items/".$id);
    }

    public function getItem($id){
        return $this->get("items/$id?expand=all");
    }

    public function getHandle($prefix, $suffix){
        return $this->get("handle/".$prefix."/".$suffix."?expand=all");
    }
    /*
     * Comunidad
     */

    public function comunidadGuardar($data, $parentCommunity = null, $colecciones = null){
        unset($data['p']);
        $comunidad = null;

        $uuid = null;
        if (isset($data['uuid']) && $data['uuid']!=null) {
            $uuid = $data['uuid'];
        }

        if (!$uuid){
            $endpoint = "communities";
            if ($parentCommunity){
                $endpoint = $endpoint."/".$parentCommunity."/".$endpoint;
            }
            $comunidad = $this->post($endpoint, $data);
            $uuid = $comunidad['uuid'];
        }else{
            $response = $this->put('communities/'.$uuid, $data);
        }

        if ($colecciones){
            foreach ($colecciones as $coleccion){
                $coleccionData = array(
                    "name" => $coleccion,
                    "shortDescription" => "",
                    "introductoryText" => "",
                    "sidebarText" => "",
                    "copyrightText" => ""
                );
                $coleccionResponse = $this->post("communities/".$uuid."/collections", $coleccionData);
            }
        }
        return $this->get("communities/".$uuid."?expand=all");
    }

    public function comunidadEliminar($uuid){
        return $this->delete('communities/' . $uuid);
    }

    public function getComunidades($uuid = null){
        if ($uuid){
            return $this->get("communities/$uuid?expand=all");
        } else {
            return $this->get('hierarchy?expand=all');
        }
    }

    public function getTopComunidades(){

        $respuesta = $this->get("communities/top-communities");

        return $respuesta;
    }

    public function getItems($offset=0, $limit = 10){

        $data = array (
            'offset' => $offset,
            'limit' => $limit,
            'expand' => 'metadata'
        );

        $respuesta = $this->get("items?".http_build_query($data));

        return array(
            'items' => $respuesta,
        );

    }

    public function getUltimosItems($comunidad, $limit = 10, $offset = 0, $coleccion = null){
        $data = array (
            'q' => '*:*',
            'start' => $offset,
            'rows' => $limit,
            'fl' => 'dc.description,handle,search.resourcetype,search.resourceid,title',
            'fq' => [
                'NOT(withdrawn:true)',
                'NOT(discoverable:false)',
                'search.resourcetype:2'
            ],
            'wt' => 'json',
            'sort' => 'dc.date.accessioned_dt desc'
        );

        if ($comunidad){
            $data['fq'][] = 'location.comm:'. $comunidad;
        }

        if ($coleccion){
            $data['fq'][] = 'location.coll:'. $coleccion;
        }

        $valores = $this->get($this->buildQuery($data, 'search/select?'), 'solr');

        return $valores['response']['docs'];
    }

    public function filtro($texto,$offset=0, $limit = 10, $coleccion = ''){

        $data = array(
            'query_field[]' => '*',
            'query_op[]' => 'contains',
            'query_val[]' => '%'.$texto.'%',
            'collSel[]' => $coleccion,
            'limit' => $limit,
            'offset' => $offset,
            'expand' => 'parentCollection,metadata',
            'filters' => 'none',
            'show_fields[]' => 'dc.contributor.author',
        );

        $items = $this->get("filtered-items?".http_build_query($data));

        return $items['items'];
    }



    /*
     * Otros
     */

    public function getPublicacionesAnios(){

        $data = array (
            'q' => '*:*',
            'rows' => '0',
            'wt' => 'json',
            'indent' => 'true',
            'facet' => 'true',
            'facet.field' => 'dateIssued.year',
            'f.dateIssued.year.facet.sort' => 'index'
        );

        $valores = $this->get($this->buildQuery($data, 'search/select?'), 'solr')['facet_counts']['facet_fields']['dateIssued.year'];

        $cantidades = array_values(array_filter($valores, function ($input) {return $input & 1;}, ARRAY_FILTER_USE_KEY));
        $anios = array_values(array_filter($valores, function ($input) {return !($input & 1);}, ARRAY_FILTER_USE_KEY));

        $respuesta = array();
        for ($i = 0; $i < count($anios); $i++){
            $respuesta[] = array(
                'nombre' => $anios[$i],
                'cantidad' => $cantidades[$i]
            );
        }

        return array(
            'listado' => $respuesta
        );

    }

    public function getAuthors($texto, $offset=0, $limit = 10){

        $data = array (
            'q' => '*:*',
            'fl' => 'handle,search.resourcetype,search.resourceid',
            'fq' => [
                'NOT(withdrawn:true)',
                'NOT(discoverable:false)'
            ],
            'start' => '0',
            'rows' => '0',
            'facet.field' => 'bi_2_dis_filter',
            'facet' => 'true',
            'facet.prefix' => strtolower($texto),
            'f.bi_2_dis_filter.facet.limit' => $limit,
            'f.bi_2_dis_filter.facet.sort' => 'index',
            'facet.mincount' => '1',
            'facet.offset' => $offset,
            'wt' => 'json'
        );

        $respuesta = $this->get($this->buildQuery($data, 'search/select?'),"solr");

        $items = $respuesta['facet_counts']['facet_fields']['bi_2_dis_filter'];

        $cantidades = array_values(array_filter($items, function ($input) {return $input & 1;}, ARRAY_FILTER_USE_KEY));
        $personas = array_values(array_filter($items, function ($input) {return !($input & 1);}, ARRAY_FILTER_USE_KEY));

        $autores = array();
        for ($i = 0; $i < count($personas); $i++){
            $autores[] = array(
                'nombre' => substr($personas[$i],strpos($personas[$i],'|')+4),
                'cantidad' => $cantidades[$i]
            );
        }

        return array(
            'listado' => $autores,
            'mas' => boolval(count($autores) === $limit)
        );

    }

    public function getItemsAutor($autor = '', $limit = 10, $offset = 0){
        $data = array (
            'q' => '*:*',
            'start' => $offset,
            'rows' => $limit,
            'fl' => 'dc.description,handle,search.resourcetype,search.resourceid,title',
            'fq' => [
                'NOT(withdrawn:true)',
                'NOT(discoverable:false)',
                '{!field f=bi_2_dis_value_filter}'.$autor,
                'search.resourcetype:2'
            ],
            'wt' => 'json',
            'sort' => 'bi_sort_1_sort asc'
        );
        $valores = $this->get($this->buildQuery($data, 'search/select?'), 'solr');

        return $valores['response']['docs'];
    }

    public function fullsearch($texto = '', $comunidad = null,$offset = 0, $limit = 10){
        $data = array (
            'q' => $texto,
            'fl' => 'dc.description,handle,search.resourcetype,search.resourceid,title',
            'spellcheck.q' => $texto,
            'spellcheck.collate' => 'true',
            'spellcheck' => 'true',
            'fq' => [
                'NOT(withdrawn:true)',
                'NOT(discoverable:false)',
                'search.resourcetype:2'
            ],
            'start' => $offset,
            'rows' => $limit,
            'sort' => 'score desc',
            'hl' => 'true',
            'hl.usePhraseHighlighter' => 'true',
            'hl.fl' => 'fulltext_hl',
            'f.dc.contributor.author_hl.hl.fragsize' => '0',
            'f.dc.contributor.author_hl_hl.snippets' => '5',
            'f.dc.description_hl.hl.fragsize' => '250',
            'f.dc.description_hl.hl.snippets' => '2',
            'f.dc.description.abstract_hl.hl.fragsize' => '250',
            'f.dc.description.abstract_hl.hl.snippets' => '2',
            'f.dc.title_hl.hl.fragsize' => '0',
            'f.dc.title_hl.hl.snippets' => '5',
            'f.fulltext_hl.hl.fragsize' => '250',
            'f.fulltext_hl.hl.snippets' => '2',
            'wt' => 'json'
        );
        if ($comunidad){
            $data['fq'][] = 'location.comm:'. $comunidad;
        }

        $respuesta = $this->get($this->buildQuery($data, 'search/select?'),"solr");

        return $respuesta['response']['docs'];
    }
    public function getSchemas(){
        return $this->get("registries/schema");
    }

    public function getUrlArchivo($endpoint){
        return "http://" . $this->servidor . ":" . $this->puerto . $endpoint;
    }

    /*
     * Utiles
     */
    private function buildQuery($data, $prefix = ''){
        $query = http_build_query($data, null, '&');
        $string = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $query); //foo=x&foo=y
        return $prefix.$string;
    }

    private function getUrl($endpoint, $prefix = 'rest'){
        return "http://" . $this->servidor . ":" . $this->puerto . "/".$prefix ."/". $endpoint;
    }

    /**
     * @return string
     */
    public function getRealUrl(): string
    {
        return $this->url;
    }



}