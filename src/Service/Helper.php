<?php
/**
 * Created by PhpStorm.
 * User: agustin
 * Date: 13/03/17
 * Time: 18:47
 */


namespace App\Service;

use App\Exceptions\UsuarioException;
use App\Exceptions\AppException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Helper
{
    private $logger;
    private $context;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage,LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
    }

    public function error($e)
    {
        $mensaje =$e->getMessage();
        if ($e instanceof AppException || $e instanceof UsuarioException){
            if ($e->getException()!=null){
                $mensaje.=" - ".$e->getException()->getMessage().' | archivo: '.
                    $e->getException()->getFile().'(linea: '.$e->getException()->getLine().')';
            }
        }

        $this->logger->error($mensaje);
    }

    public function info($mensaje)
    {
        $this->logger->info($mensaje);
    }

}