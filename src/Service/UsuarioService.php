<?php


// src/App/FileUploader.php
namespace App\Service;

use App\Entity\Usuario;
use App\Exceptions\AppException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioService
{

    private $entityManager;

    private $usuarioRepository;

    private $helper;

    private $encoder;

    public function __construct(EntityManagerInterface $entityManager,Helper $helper, UserPasswordEncoderInterface $encoder)
    {
        $this->entityManager = $entityManager;
        $this->usuarioRepository = $this->entityManager->getRepository(Usuario::class);
        $this->helper = $helper;
        $this->encoder = $encoder;
    }

    public function alta($nombre, $apellido, $email, $roles, $password=null, $passwordRetry=null)
    {

        $this->entityManager->beginTransaction();

        try{

            $this->validar(null, $nombre, $apellido, $email, $password, $passwordRetry);

            $oUsuario = new Usuario();

            $oUsuario->setEmail($email);

            $oUsuario->setNombre($nombre);
            $oUsuario->setApellido($apellido);


            $password = $this->encoder->encodePassword($oUsuario, $password);
            $oUsuario->setPassword($password);

            $oUsuario->setRoles($roles);

            $this->entityManager->persist($oUsuario);
            $this->entityManager->flush();

            $this->entityManager->commit();
            return $oUsuario;
         }catch(AppException $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            throw $e;
        }catch(Exception $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            new AppException("Hubo problemas para crear el Usuario. Intente nuevamente", "danger",$e);
        }
    }

    public function modificar($id, $nombre, $apellido, $email,  $roles, $password = null, $passwordRetry = null)
    {

        $this->entityManager->beginTransaction();

        try{

            $oUsuario = $this->usuarioRepository->find($id);

            /**
             * @var $oUsuario Usuario
             */

            $this->validar($oUsuario, $nombre, $apellido, $email, $password, $passwordRetry);

            $oUsuario->setNombre($nombre);
            $oUsuario->setApellido($apellido);

            if ($password!=null && $password!='') {
                $password = $this->encoder->encodePassword($oUsuario, $password);
                $oUsuario->setPassword($password);
            }
            $oUsuario->setEmail($email);

            $oUsuario->setRoles($roles);

            $this->entityManager->flush();
            $this->entityManager->commit();
            return $oUsuario;
        }catch(AppException $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            throw $e;
        }catch(Exception $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            new AppException("Hubo problemas para modificar el usuario. Intente nuevamente", "danger",$e);
        }
    }


    public function cambiarClave($id,$password, $passwordRetry)
    {

        $this->entityManager->beginTransaction();

        try{

            $oUsuario = $this->usuarioRepository->find($id);

            /**
             * @var $oUsuario Usuario
             */

            if ($password===''){
                throw new AppException("La clave no puede estar vacía");
            }

            $this->validar($oUsuario, $oUsuario->getNombre(), $oUsuario->getApellido(), $oUsuario->getEmail(), $password, $passwordRetry);

            $password = $this->encoder->encodePassword($oUsuario, $password);
            $oUsuario->setPassword($password);

            $this->entityManager->flush();
            $this->entityManager->commit();
        }catch(AppException $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            throw $e;
        }catch(Exception $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            new AppException("Hubo problemas para modificar la clave. Intente nuevamente", "danger",$e);
        }
    }

    public function baja($id)
    {

        $this->entityManager->beginTransaction();
        try{
            $oUsuario = $this->usuarioRepository->find($id);

            /**
             * @var $oUsuario Usuario
             */
            $this->entityManager->remove($oUsuario);

            $this->entityManager->flush();
            $this->entityManager->commit();
        }catch(Exception $e){
            $this->entityManager->rollback();
            $this->helper->error($e);
            throw new AppException("Hubo problemas para eliminar el Usuario. Intente nuevamente", "danger",$e);
        }


    }

    public function eliminar($id)
    {

        $this->entityManager->beginTransaction();
        try{
            $oUsuario = $this->usuarioRepository->find($id);

            $this->baja($id);

            $this->entityManager->flush();
            $this->entityManager->commit();
        }catch(Exception $e){
            $this->entityManager->rollback();
            $this->helper->info($e->getMessage().$e->getLine());
            $this->helper->error($e);
            throw new AppException("Hubo problemas para eliminar el Usuario. Intente nuevamente", "danger",$e);
        }


    }

    private function validar($oUsuario,$nombre, $apellido, $email, $password, $passwordRetry){

        /**
         * @var $oUsuario Usuario
         */
        if (!$oUsuario || $oUsuario->getEmail()!=$email){
            if ($this->usuarioRepository->findOneBy(array('email' => $email)))
                throw new AppException("El email ya está en uso");
        }

        if (strlen($nombre)==0)
            throw new AppException("El nombre no puede estar vacío");

        if (strlen($apellido)==0)
            throw new AppException("El apellido no puede estar vacío");

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new AppException("El email no es válido");

        if ($password!=$passwordRetry)
            throw new AppException("Las claves no coinciden");


    }

}
