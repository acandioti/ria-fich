<?php
/**
 * Created by PhpStorm.
 * User: agustin
 * Date: 13/03/17
 * Time: 18:47
 */


namespace App\Service;

use App\DTO\Item;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Utiles
{
    protected $requestStack;
    protected $paginator;

    public function __construct(RequestStack $requestStack, PaginatorInterface $paginator)
    {
        $this->requestStack = $requestStack;
        $this->paginator = $paginator;
    }

    public function get($nombreVariable)
    {
        $request = $this->requestStack->getCurrentRequest();

        $matches    = array();
        preg_match('/(.*)\\\Controller\\\(.*)Controller::(.*)Action/', $request->attributes->get('_controller'), $matches);

        if ($request->isMethod('POST')) {
            $variable = $request->get($nombreVariable);
            $request->getSession()->set($matches[2].'.'.$nombreVariable,$variable);
        }else{
            $variable = $request->getSession()->get($matches[2].'.'.$nombreVariable);
        }

        return $variable;
    }

    public function jsonPaginator($query, $page = 1, $limit = 10)
    {

        if ($page<1) $page = 1;
        if ($limit<1) $limit = 1;

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit
        );

        return
            array(
                'paginado'=> array(
                    'pagina_actual' => $pagination->getCurrentPageNumber(),
                    'items_por_pagina' => $pagination->getItemNumberPerPage(),
                    'total_paginas' => intval(ceil($pagination->getTotalItemCount() / $pagination->getItemNumberPerPage())),
                    'total_items' => $pagination->getTotalItemCount(),
                ),
                'listado' => $pagination->getItems(),
            );
    }

    public function convertListItem($originalItems){
        $newItems = array();
        foreach ($originalItems as $item){
            $newItems[] = new Item($item);
        }
        return $newItems;
    }

}