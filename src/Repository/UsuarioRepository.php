<?php

namespace App\Repository;

use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Usuario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usuario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usuario[]    findAll()
 * @method Usuario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsuarioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuario::class);
    }

    public function filtro($email, $nombre, $apellido)
    {

        $em = $this->getEntityManager();

        $sql="SELECT u FROM App:Usuario u WHERE 1=1";

        if($email!=null){
            $sql.=" AND u.emailCanonical LIKE :emailCanonical";
        }
        if ($nombre!=null){
            $sql.=" AND LOWER(u.nombre) LIKE :nombre";
        }
        if ($apellido!=null){
            $sql.=" AND LOWER(u.apellido) LIKE :apellido";
        }

        $query= $em->createQuery($sql);

        if($email!=null){
            $query->setParameter('emailCanonical','%'.strtolower($email).'%');
        }
        if($nombre!=null){
            $query->setParameter('nombre','%'.strtolower($nombre).'%');
        }
        if($apellido!=null){
            $query->setParameter('apellido','%'.strtolower($apellido).'%');
        }

        return $query;

    }
}
