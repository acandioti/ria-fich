<?php


namespace App\DTO;


class Item
{

    public $itemData;

    public $id;

    public $handle;

    public $titulo;

    public $autores;

    public $tema;

    public $descripcion;

    public $filiacion;

    public $coleccion;

    public $comunidades;

    public $editores;

    public $colaboradores;

    public $fechaPublicacion;

    public $fechaFinalizacionEmbargo;

    public $nivelAcceso;

    public $archivos;

    public $metadatos;

    /**
     * Item constructor.
     * @param $titulo
     */
    public function __construct($item)
    {
        //array_filter()
        $this->itemData = $item;
        if (isset($item['uuid'])){
            $this->id = $item['uuid'];
            $this->handle = explode('/',$item['handle']);
            $this->titulo = $this->getValue($item['metadata'],'dc.title');
            $this->autores = $this->getValues($item['metadata'],'dc.creator');
            $this->tema = $this->getValue($item['metadata'],'dc.subject');
            $this->descripcion = $this->getValue($item['metadata'],'dc.description');
            $this->filiacion = $this->getValue($item['metadata'],'dc.description',1);
            $this->coleccion = $item['parentCollection'];
            $this->comunidades = $item['parentCommunityList'];
            $this->editores = $this->getValues($item['metadata'],'dc.publisher');
            $this->fechaPublicacion = $this->getValue($item['metadata'],'dc.date');
            $this->fechaFinalizacionEmbargo = $this->getValue($item['metadata'],'dc.date',1);
            $this->nivelAcceso = $this->getValue($item['metadata'],'dc.rights',0);
            $this->archivos = $item['bitstreams'];
            $this->metadatos = $item['metadata'];
        }elseif(isset($item['search.resourceid'])){
            $this->id = $item['search.resourceid'];
            $this->handle = explode('/',$item['handle']);
            $this->titulo = $item['title'][0];
            if (isset($item['dc.description'])){
                $this->descripcion = $item['dc.description'][0];
            }else{
                $this->descripcion = '';
            }

        }

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return false|string[]
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * @return mixed|null
     */
    public function getTitulo(): ?mixed
    {
        return $this->titulo;
    }

    /**
     * @return array
     */
    public function getAutores(): array
    {
        return $this->autores;
    }

    /**
     * @return mixed|null
     */
    public function getTema(): ?mixed
    {
        return $this->tema;
    }

    /**
     * @return mixed|null
     */
    public function getDescripcion(): ?mixed
    {
        return $this->descripcion;
    }

    /**
     * @return mixed|null
     */
    public function getFiliacion(): ?mixed
    {
        return $this->filiacion;
    }

    /**
     * @return mixed
     */
    public function getColeccion()
    {
        return $this->coleccion;
    }

    /**
     * @return mixed
     */
    public function getComunidades()
    {
        return $this->comunidades;
    }

    /**
     * @return array
     */
    public function getEditores(): array
    {
        return $this->editores;
    }

    /**
     * @return mixed
     */
    public function getColaboradores()
    {
        return $this->colaboradores;
    }

    /**
     * @return mixed|null
     */
    public function getFechaPublicacion(): ?mixed
    {
        return $this->fechaPublicacion;
    }

    /**
     * @return mixed|null
     */
    public function getFechaFinalizacionEmbargo(): ?mixed
    {
        return $this->fechaFinalizacionEmbargo;
    }

    /**
     * @return mixed|null
     */
    public function getNivelAcceso(): ?mixed
    {
        return $this->nivelAcceso;
    }

    /**
     * @return mixed
     */
    public function getArchivos()
    {
        return $this->archivos;
    }

    /**
     * @return mixed
     */
    public function getMetadatos()
    {
        return $this->metadatos;
    }

    private function getValues($array, $clave ){
        $filtradas = array_filter($array, function($var) use ($clave) { return $var['key']===$clave;});
        return array_column($filtradas, 'value');
    }

    private function getValue($array, $clave, $index = 0){
        $values = $this->getValues($array, $clave);
        if (isset($values[$index])){
            return $values[$index];
        }else{
            return null;
        }
    }
}