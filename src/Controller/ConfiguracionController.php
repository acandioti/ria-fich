<?php
/**
 * Created by PhpStorm.
 * User: emanuel
 * Date: 27/08/18
 * Time: 22:08
 */

namespace App\Controller;

use App\Service\DSpaceService;
use App\Exceptions\DSpaceException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Configuracion controller.
 *
 * @Route("configuracion")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class ConfiguracionController extends AbstractController
{
    private $dspaceService;

    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService)
    {
        $this->dspaceService = $dspaceService;
    }
    /**
     * Nuevo configuracion entity.
     *
     * @Route("/", name="configuracion_abm", methods={"GET", "POST"})
     */
    public function abmAction(Request $request)
    {
        $Configuraciones = $this->getDoctrine()->getRepository('App:Configuracion')->findAll();
        if ($request->isMethod('POST')){
            try{
                $valores = array();
                foreach ($Configuraciones as $configuracion){
                    $valores[$configuracion->getId()] = $request->get($configuracion->getId());
                }
                $this->getDoctrine()->getRepository('App:Configuracion')->abm($valores);
                $this->addFlash('success','La configuración se ha guardado con éxito');
            } catch (DSpaceException $e) {
                $this->get('helper')->error($e);
                $this->addFlash($e->getTipo(), $e->getMessage());
            }

        }

        return $this->render('Configuracion/abm.html.twig', array(
            'Configuraciones' => $Configuraciones,
        ));
    }


}