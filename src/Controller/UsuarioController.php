<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Exceptions\AppException;
use App\Exceptions\UsuarioException;
use App\Service\DSpaceService;
use App\Exceptions\DSpaceException;
use App\Service\Helper;
use App\Service\UsuarioService;
use App\Service\Utiles;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("usuario")
 */
class UsuarioController extends AbstractController
{
    private $dspaceService;
    private $usuarioService;
    private $utiles;
    private $helper;
    private $paginator;
    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService, UsuarioService $usuarioService, Utiles $utiles, Helper $helper, PaginatorInterface $paginator)
    {
        $this->dspaceService = $dspaceService;
        $this->usuarioService = $usuarioService;
        $this->utiles = $utiles;
        $this->helper = $helper;
        $this->paginator = $paginator;
    }
    /**
     * @Route("/", name="usuario_inicio")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->redirectToRoute("usuario_listado");
    }

    /**
     * @param Request $request
     * @Route("/nuevo", name="usuario_nuevo")
     * @Route("/{id}/editar", name="usuario_editar")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @return Response
     */
    public function abmAction(Request $request, Usuario $Usuario=null)
    {

        //$roles= $this->getParameter('security.role_hierarchy.roles');

        if ($request->isMethod('POST')){
            $password = $request->get('password');
            $passwordConfirm = $request->get('passwordConfirm');
            $nombre = $request->get('nombre');
            $apellido = $request->get('apellido');
            $email =  $request->get('email');
            $rol = $request->get('rol');

            try{
                if ($Usuario==null) {
                    $Usuario = $this->usuarioService->alta($nombre, $apellido, $email,array($rol), $password, $passwordConfirm);
                    $this->addFlash('success', 'El usuario se ha registrado con éxito');
                    return $this->redirectToRoute('usuario_listado');
                }else{
                    $Usuario = $this->usuarioService->modificar($Usuario->getId(),$nombre, $apellido, $email,array($rol), $password, $passwordConfirm);
                    $this->addFlash('success', 'El Usuario se modificó con éxito');
                }
            }catch(AppException $e){
                $this->addFlash($e->getTipo(), $e->getMessage());
            }
        }
        return $this->render('usuario/abm.html.twig', array(
            'Usuario' => $Usuario,
        ));
   }

    /**
     * @param Request $request
     * @Route("/cambiar-clave", name="usuario_cambiar_clave")
     * @Security("has_role('ROLE_USER')")
     * @return Response
     */
    public function cambiarClaveAction(Request $request)
    {
        if ($request->isMethod('POST')){
            $password = $request->get('password');
            $passwordConfirm = $request->get('passwordConfirm');

            try{
                $this->usuarioService->cambiarClave($this->getUser()->getId(), $password, $passwordConfirm);
                $this->addFlash('success', 'La clave ha sido modificada con éxito');
            }catch(AppException $e){
                $this->addFlash($e->getTipo(), $e->getMessage());
            }
        }
        return $this->render('usuario/cambiar-clave.html.twig', array(
        ));
    }

    /**
     * Eliminar cuestionario entity.
     * @Route("/eliminar", name="usuario_eliminar", methods={"GET","POST"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function eliminarAction(Request $request)
    {

        try{
            $this->usuarioService->baja($request->get('id'));
            $this->addFlash('success', 'Usuario eliminado');
            return $this->redirectToRoute('usuario_listado');
        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $this->addFlash($e->getTipo(), $e->getMessage());
        }

        return $this->redirectToRoute('comunidad_listado');
    }

    /**
     * @param Request $request
     * @Route("/listado/{pagina}", name="usuario_listado", requirements={"pagina": "\d+"}, methods={"GET", "POST"})
     * @Route("/listado/{pagina}/{limite}", name="usuario_listado", requirements={"pagina": "\d+"}, methods={"GET", "POST"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @return Response
     */
    public function listadoAction(Request $request,$pagina=1,$limite=10)
    {
        $em = $this->getDoctrine()->getManager();
        $email = $request->get('email','');
        $nombre = $request->get("nombre",'');
        $apellido = $request->get("apellido",'');

        $usuarios = $this->paginator->paginate(
            $this->getDoctrine()->getRepository(Usuario::class)->filtro($email, $nombre, $apellido),
            $pagina,
            $limite
        );


        if (!$request->isXmlHttpRequest()){
            return $this->render('usuario/filtrado.html.twig', array(
                'usuarios' => $usuarios,
                'email' => $email,
                'nombre' => $nombre,
                'apellido' => $apellido
            ));
        }else{
            return $this->render('usuario/listado.html.twig', array(
                'usuarios' => $usuarios,
            ));
        }
    }

}
