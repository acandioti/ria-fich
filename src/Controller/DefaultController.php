<?php

namespace App\Controller;

use App\Service\DSpaceService;
use App\Service\Utiles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    private $dspaceService;
    private $utiles;

    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService, Utiles $utiles)
    {
        $this->dspaceService = $dspaceService;
        $this->utiles = $utiles;
    }

    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {


        $comunidades = $this->dspaceService->getTopComunidades();

        //$tipos = $this->getDoctrine()->getRepository('App:Coleccion')->findAll();

        $items = $this->dspaceService->getUltimosItems(null, 6, 0);
        $items = $this->utiles->convertListItem($items);

        foreach ($comunidades as &$comunidad){
            $comunidad['items'] = $this->utiles->convertListItem($this->dspaceService->getUltimosItems($comunidad['uuid'], 6, 0));
        }
;
        return $this->render('Default/index.html.twig', array(
            'items' => $items,
            'comunidades' => $comunidades,
            //'anios' => $anios['listado'],
            //'tipos' => $tipos
        ));

    }
}