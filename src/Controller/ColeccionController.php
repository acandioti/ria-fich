<?php
namespace App\Controller;

use App\Service\DSpaceService;
use App\Exceptions\DSpaceException;
use App\Service\Helper;
use App\Service\Utiles;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ColeccionController
 * @route("coleccion")
 */
class ColeccionController extends AbstractController
{

    private $dspaceService;
    private $helper;
    private $utiles;
    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService, Helper $helper, Utiles $utiles)
    {
        $this->dspaceService = $dspaceService;
        $this->helper = $helper;
        $this->utiles = $utiles;
    }

    /**
     *
     * @Route("/{prefix}/{suffix}", name="coleccion_vista", methods={"GET", "POST"})
     * @Route("/handle", name="coleccion_handle", methods={"GET"}, defaults={"prefix": 0, "suffix": 0})
     */
    public function vistaAction(Request $request, string $prefix, string $suffix){

        if ($prefix == "0"){
            $prefix = $request->query->get('prefix',0);
            $suffix = $request->query->get('suffix',0);
        }

        $coleccion = $this->dspaceService->getHandle($prefix, $suffix);
        if ($request->get('search')){
            $items = $this->dspaceService->filtro(
                $request->get('search',''),
                $request->get('offset',0),
                $request->get('limit',10),
                $coleccion['uuid']
            );
        }else{
            $items = $this->dspaceService->getUltimosItems(null,
                $request->get('limit',10),
                $request->get('offset',0),
                $coleccion['uuid']
            );
        }
        return $this->render('coleccion/vista.html.twig', array(
            'coleccion' => $coleccion,
            'items' => $this->utiles->convertListItem($items)
        ));
    }

}