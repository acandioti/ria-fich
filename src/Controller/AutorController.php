<?php

namespace App\Controller;

use App\Service\DSpaceService;
use App\Exceptions\DSpaceException;
use App\Service\Utiles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AutorController
 * @Route("autor")
 */
class AutorController extends AbstractController
{
    private $dspaceService;
    private $utiles;

    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService, Utiles $utiles)
    {
        $this->dspaceService = $dspaceService;
        $this->utiles = $utiles;
    }
    /**
     * Nuevo configuracion entity.
     *
     * @Route("/", name="autor_home", methods={"GET", "POST"})
     */
    public function abmAction(Request $request)
    {
        return $this->redirectToRoute("item_listado");
    }


    /**
     * Listado item entities.
     *
     * @param Request $request
     * @Route("/listado", name="autor_listado", methods={"GET", "POST"})
     * @return Response
     */
    public function listadoAction(Request $request)
    {
        try{

            $autores = $this->dspaceService->getAuthors(
                $request->get('search', ''),
                $request->get('offset',0),
                $request->get('limit',10)
            );

        } catch (DSpaceException $e) {
            $this->get('helper')->error($e);
            $this->addFlash($e->getTipo(), $e->getMessage());
        }
        return $this->render('autor/listado.html.twig', array(
            'autores' => $autores['listado'],
            'mas' => $autores['mas'],
        ));
    }

    /**
     *
     * @Route("/items", name="autor_vista", methods={"GET", "POST"})
     */
    public function vistaAction(Request $request)
    {
        $items = $this->dspaceService->getItemsAutor($request->get('autor',''),
            $request->get('limit',10),
            $request->get('offset',0)
        );

        return $this->render('autor/vista.html.twig', array(
            'items' => $this->utiles->convertListItem($items)
        ));
    }

}