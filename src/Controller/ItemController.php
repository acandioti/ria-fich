<?php

namespace App\Controller;



use App\DTO\Item;
use App\Service\DSpaceService;
use App\Entity\Configuracion;
use App\Exceptions\DSpaceException;
use App\Service\Helper;
use App\Service\Utiles;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Zend\Code\Scanner\Util;

/**
 * Class ItemController
 * @Route("item")
 */
class ItemController extends AbstractController
{
    private $dspaceService;
    private $helper;
    private $utiles;
    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService, Helper $helper, Utiles $utiles)
    {
        $this->dspaceService = $dspaceService;
        $this->helper = $helper;
        $this->utiles = $utiles;
    }
    /**
     * Nuevo configuracion entity.
     *
     * @Route("/", name="item_home", methods={"GET", "POST"})
     */
    public function abmAction(Request $request)
    {
        return $this->redirectToRoute("item_listado");
    }


    /**
     * Listado item entities.
     *
     * @param Request $request
     * @Route("/listado", name="item_listado", methods={"GET", "POST"})
     * @return Response
     */
    public function listadoAction(Request $request)
    {
        try{
            $items = $this->dspaceService->filtro(
                $request->get('search', ''),
                $request->get('offset',0),
                $request->get('limit',10)
            );

        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $this->addFlash($e->getTipo(), $e->getMessage());
            $items = [];
        }
        return $this->render('item/busqueda.html.twig', array(
            'items' => $this->utiles->convertListItem($items),
        ));
    }

    /**
     * Nuevo item entity.
     *
     * @Route("/nuevo", name="item_nuevo")
     * @Route("/{id}/editar", name="item_editar", methods={"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function nuevoAction(Request $request, $id=null)
    {
        $schemas = $this->dspaceService->getSchemas();
        $comunidades = $this->dspaceService->getComunidades();

        $tipoColecciones = $this->getDoctrine()->getRepository('App:Coleccion')->findAll();
        $licencia = $this->getDoctrine()->getRepository(Configuracion::class)->find(Configuracion::licencia);

        $arrayColecciones = $this->get('serializer')->normalize($tipoColecciones);

        $item = null;

        if ($id){

            try{

                $item = $this->dspaceService->getItem($id);

            } catch (DSpaceException $e) {
                $this->get('helper')->error($e);
                $this->addFlash($e->getTipo(), $e->getMessage());
            }
        }

        return $this->render('item/abm.html.twig', array(
            'schemas' => $schemas,
            'comunidades' => $comunidades['community'],
            'tipoColecciones' => $arrayColecciones,
            'item' => $item,
            'licencia' => $licencia,
        ));
    }


    /**
     * Guarda item entity.
     *
     * @Route("/guardar", name="item_guardar", methods={"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function guardarAction(Request $request){

        $item = json_decode($request->get('item'), true);
        $archivos = json_decode($request->get('archivos'), true);

        $mensaje = 'El item ha sido guardado correctamente';

        $response = array();

        try{

            $response = $this->dspaceService->itemGuardar($item, $archivos);

            $mensajeTipo = 1;
        }catch (DSpaceException $e){
            $this->helper->error($e);
            $mensaje = $e->getMessage();
            $mensajeTipo = $e->getTipo();
        }

        return $this->json(array(
            'mensaje' => $mensaje,
            'mensajeTipo' => $mensajeTipo,
            'item' => $response
        ));
    }


    /**
     * Guarda item entity.
     *
     * @Route("/subir", name="item_subir", methods={"POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function subirArchivosAction(Request $request){

        $this->helper->info('Sube el archivo');

        $archivos = [];
        $mensaje = 'El Item fue publicado correctamente';
        $mensajeTipo = 'success';

        try{

            foreach($request->files->all() as $file){
                /**
                 * @var $file UploadedFile
                 */
                $ruta = $this->getParameter('target_dir');

                $archivos[]=array(
                    'nombre' => $file->getClientOriginalName(),
                    'ruta' => $ruta.$file->getFilename(),
                    'tipo' => $file->getMimeType(),
                    'descripcion' => ""
                );
                $file->move($ruta);

            }

        }catch(Exception $e){
            $this->helper->error($e);
            $mensaje = $e->getMessage();
            $mensajeTipo = $e->getTipo();
        }

        return $this->json(array(
            'mensaje' => $mensaje,
            'mensajeTipo' => $mensajeTipo,
            'archivos' => $archivos,
        ));
    }



    /**
     * Obtiene el bitstream.
     *
     * @Route("/bitstream/{id}/{nombre}", name="obtener_archivo", methods={"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function obtenerArchivoAction(Request $request, $id = null, $nombre = null)
    {
        $this->helper->info("Obtener archivo");
        $this->helper->info("uuid" . $id);

        try{
            $response = $this->dspaceService->getFile($id);

            $this->helper->info(gettype($response));

            return $this->file('/tmp/'.$id, $nombre);
        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $this->addFlash($e->getTipo(), $e->getMessage());
            $item = [];
        }
        return $this->redirectToRoute('item_listado');
    }

    /**
     * Editar item entity.
     *
     * @Route("/eliminar", name="item_eliminar", methods={"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function eliminarAction(Request $request)
    {

        $id = $request->get('id');

        $this->helper->info("uuid" . $id);
        try{

            $this->dspaceService->itemEliminar($id);
            $mensaje="El item ha sido eliminado correctamente";
            $mensajeTipo = "success";
            $this->addFlash($mensajeTipo, $mensaje);
        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $mensaje = $e->getMessage();
            $mensajeTipo = $e->getTipo();
        }

        return $this->json(array(
            'mensaje' => $mensaje,
            'mensajeTipo' => $mensajeTipo,
        ));
    }

    /**
     * Eliminar item entity.
     *
     * @Route("/eliminarArchivo/{id}", name="archivo_eliminar", methods={"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function eliminarArchivoAction(Request $request, $id = null)
    {
        $this->helper->info('Eliminar archivo ' . $id);

        try{
            $this->dspaceService->archivoEliminar($id);
            $mensaje = 'El Item fue eliminado correctamente';
            $mensajeTipo = 'success';
        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $mensaje = $e->getMessage();
            $mensajeTipo = $e->getTipo();
        }

        return $this->json(array(
            'mensaje' => $mensaje,
            'mensajeTipo' => $mensajeTipo,
        ));
    }


    /**
     * Editar item entity.
     *
     * @Route("/{prefix}/{suffix}", name="item_vista", methods={"GET", "POST"})
     */
    public function vistaAction(Request $request, $prefix, $suffix)
    {

        $item = $this->dspaceService->getHandle($prefix, $suffix);
        $item = new Item($item);
        return $this->render('item/vista.html.twig', array(
            'item' => $item,
        ));
    }
}