<?php
namespace App\Controller;

use App\Service\DSpaceService;
use App\Exceptions\DSpaceException;
use App\Service\Helper;
use App\Service\Utiles;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ComunidadController
 * @route("comunidad")
 */
class ComunidadController extends AbstractController
{

    private $dspaceService;
    private $helper;
    private $utiles;
    /**
     * DefaultController constructor.
     */
    public function __construct(DSpaceService $dspaceService, Helper $helper, Utiles $utiles)
    {
        $this->dspaceService = $dspaceService;
        $this->helper = $helper;
        $this->utiles = $utiles;
    }
    /**
     * Nuevo configuracion entity.
     *
     * @Route("/", name="communities", methods={"GET", "POST"})
     */
    public function defaultAction(Request $request)
    {
        return $this->redirectToRoute("comunidad_listado");
    }


    /**
     * Listado comunidad entities.
     *
     * @param Request $request
     * @Route("/listado", name="comunidad_listado", methods={"GET", "POST"})
     * @return Response
     */
    public function listadoAction(Request $request)
    {
        try {

            $comunidades = $this->dspaceService->getComunidades();
        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $this->addFlash($e->getTipo(), $e->getMessage());
            $comunidades = [];
        }

        return $this->render('comunidad/listado.html.twig', array(
            'comunidades' => $comunidades['community'],
        ));
    }


    /**
     * Nuevo comunidad entity.
     *
     * @Route("/nuevo", name="comunidad_nuevo")
     * @Route("/editar", name="comunidad_editar", methods={"GET", "POST"})
     */
    public function abmAction(Request $request)
    {

        $comunidad=null;
        $uuid = $request->get('id');
        $parentCommunity = $request->get('p');
        $data = $request->request->all();
        if ($uuid!=null){
            $comunidad = $this->dspaceService->getComunidades($uuid);
        }

        $tipoColecciones = $this->getDoctrine()->getRepository('App:Coleccion')->findAll();

        $arrayColecciones = $this->get('serializer')->serialize($tipoColecciones, 'json');

        if ($request->isMethod('POST')){
            try {

                $this->dspaceService->comunidadGuardar($data, $parentCommunity);

                $this->addFlash('success', "Comunidad guardada correctamente.");
                return $this->redirectToRoute('comunidad_listado', array());
            } catch (DSpaceException $e) {
                $this->helper->error($e);
                $this->addFlash($e->getTipo(), $e->getMessage());
            }

        }

        return $this->render('comunidad/abm.html.twig', array(
            'comunidad' => $comunidad,
            'colecciones' => $arrayColecciones,
        ));
    }


    /**
     * Guarda item entity.
     *
     * @Route("/guardar", name="comunidad_guardar", methods={"GET", "POST"})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function guardarAction(Request $request){

        $comunidad = json_decode($request->get('comunidad'), true);
        $coleccionesNuevas = json_decode($request->get('coleccionesNuevas'), true);

        $mensaje = 'La comunidad ha sido guardada correctamente';
        $response = array();


        try{

            $response = $this->dspaceService->comunidadGuardar($comunidad, $request->get('p', null), $coleccionesNuevas);
            $mensajeTipo = 1;
        }catch (DSpaceException $e){
            $this->helper->error($e);
            $mensaje = $e->getMessage();
            $mensajeTipo = $e->getTipo();
        }

        return $this->json(array(
            'mensaje' => $mensaje,
            'mensajeTipo' => $mensajeTipo,
            'comunidad' => $response
        ));
    }

    /**
     * Eliminar cuestionario entity.
     *
     * @Route("/eliminar", name="comunidad_eliminar", methods={"GET","POST"})
     */
    public function eliminarAction(Request $request)
    {

        try{
            $this->dspaceService->comunidadEliminar($request->get('id'));
            $this->addFlash('success', 'Comunidad eliminada');
            return $this->redirectToRoute('comunidad_listado');
        } catch (DSpaceException $e) {
            $this->helper->error($e);
            $this->addFlash($e->getTipo(), $e->getMessage());
        }

        return $this->redirectToRoute('comunidad_listado');
    }

    /**
     *
     * @Route("/{prefix}/{suffix}", name="comunidad_vista", methods={"GET", "POST"})
     * @Route("/handle", name="comunidad_handle", methods={"GET"}, defaults={"prefix": 0, "suffix": 0})
     */
    public function vistaAction(Request $request, string $prefix, string $suffix)
    {
        if ($prefix == "0"){
            $prefix = $request->query->get('prefix',0);
            $suffix = $request->query->get('suffix',0);
        }

        $comunidad = $this->dspaceService->getHandle($prefix, $suffix);

        if ($request->get('search')){
            $items = $this->dspaceService->fullsearch(
                $request->get('search', ''),
                $comunidad['uuid'],
                $request->get('offset', 0),
                $request->get('limit', 10)
            );
        }else{
            $items = $this->dspaceService->getUltimosItems($comunidad['uuid'],
                $request->get('limit',10),
                $request->get('offset',0)
            );
        }
        return $this->render('comunidad/vista.html.twig', array(
            'comunidad' => $comunidad,
            'items' => $this->utiles->convertListItem($items)
        ));
    }


}