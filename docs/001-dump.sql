--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.19
-- Dumped by pg_dump version 9.6.19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: coleccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.coleccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coleccion_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: coleccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.coleccion (
    id integer DEFAULT nextval('public.coleccion_seq'::regclass) NOT NULL,
    nombre character varying(50) NOT NULL,
    snrd character varying(100) NOT NULL,
    openaire character varying(100) NOT NULL
);


ALTER TABLE public.coleccion OWNER TO postgres;

--
-- Name: coleccion__version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.coleccion__version (
    id_coleccion integer NOT NULL,
    id_version integer NOT NULL
);


ALTER TABLE public.coleccion__version OWNER TO postgres;

--
-- Name: configuracion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.configuracion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuracion_seq OWNER TO postgres;

--
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configuracion (
    id integer DEFAULT nextval('public.configuracion_seq'::regclass) NOT NULL,
    nombre character varying(50) NOT NULL,
    valor text NOT NULL
);


ALTER TABLE public.configuracion OWNER TO postgres;

--
-- Name: usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_seq OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id integer DEFAULT nextval('public.usuario_seq'::regclass) NOT NULL,
    email character varying(180) NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    roles text NOT NULL,
    password character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: version_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.version_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.version_seq OWNER TO postgres;

--
-- Name: version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.version (
    id integer DEFAULT nextval('public.version_seq'::regclass) NOT NULL,
    nombre character varying(50) NOT NULL,
    metadata character varying(100) NOT NULL
);


ALTER TABLE public.version OWNER TO postgres;

--
-- Data for Name: coleccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.coleccion VALUES (1, 'Artículo', 'info:ar-repo/semantics/artículo', 'info:eu-repo/semantics/article');
INSERT INTO public.coleccion VALUES (2, 'Libro', 'info:ar-repo/semantics/libro', 'info:eu-repo/semantics/book');
INSERT INTO public.coleccion VALUES (3, 'Parte de libro', 'info:ar-repo/semantics/parte de libro', 'info:eu-repo/semantics/bookPart');
INSERT INTO public.coleccion VALUES (4, 'Documento de conferencia', 'info:ar-repo/semantics/documento de conferencia', 'info:eu-repo/semantics/conferenceObject');
INSERT INTO public.coleccion VALUES (5, 'Tesis doctoral', 'info:ar-repo/semantics/tesis doctoral', 'info:eu-repo/semantics/doctoralThesis');
INSERT INTO public.coleccion VALUES (6, 'Tesis de maestría', 'info:ar-repo/semantics/tesis de maestría', 'info:eu-repo/semantics/masterThesis');
INSERT INTO public.coleccion VALUES (7, 'Tesis de grado', 'info:ar-repo/semantics/tesis de grado', 'info:eu-repo/semantics/bachelorThesis');
INSERT INTO public.coleccion VALUES (8, 'Trabajo final de grado', 'info:ar-repo/semantics/trabajo final de grado', 'info:eu-repo/semantics/bachelorThesis');
INSERT INTO public.coleccion VALUES (9, 'Patente', 'info:ar-repo/semantics/patente', 'info:eu-repo/semantics/patent');
INSERT INTO public.coleccion VALUES (10, 'Marca', 'info:ar-repo/semantics/marca', 'info:eu-repo/semantics/patent');
INSERT INTO public.coleccion VALUES (11, 'Modelo industrial', 'info:ar-repo/semantics/modelo industrial', 'info:eu-repo/semantics/patent');
INSERT INTO public.coleccion VALUES (12, 'Modelo de utilidad', 'info:ar-repo/semantics/modelo utilidad', 'info:eu-repo/semantics/patent');
INSERT INTO public.coleccion VALUES (13, 'Documento legal', 'info:ar-repo/semantics/documento legal', 'info:eu-repo/semantics/patent');
INSERT INTO public.coleccion VALUES (14, 'Reseña artículo', 'info:ar-repo/semantics/reseña artículo', 'info:eu-repo/semantics/review');
INSERT INTO public.coleccion VALUES (15, 'Revisión literaria', 'info:ar-repo/semantics/revisión literaria', 'info:eu-repo/semantics/review');
INSERT INTO public.coleccion VALUES (16, 'Documento de trabajo', 'info:ar-repo/semantics/documento de trabajo', 'info:eu-repo/semantics/workingPaper');
INSERT INTO public.coleccion VALUES (17, 'Informe técnico', 'info:ar-repo/semantics/informe técnico', 'info:eu-repo/semantics/report');
INSERT INTO public.coleccion VALUES (18, 'Fotografía', 'info:ar-repo/semantics/fotografía', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (19, 'Plano', 'info:ar-repo/semantics/plano', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (20, 'Mapa', 'info:ar-repo/semantics/mapa', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (21, 'Diapositiva', 'info:ar-repo/semantics/diapositiva', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (22, 'Póster', 'info:ar-repo/semantics/póster', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (23, 'Imagen satelital', 'info:ar-repo/semantics/imagen satelital', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (24, 'Radiografía', 'info:ar-repo/semantics/radiografía', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (25, 'Transparencia', 'info:ar-repo/semantics/transparencia', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (26, 'Diapositiva en microscopio', 'info:ar-repo/semantics/diapositiva en microscopio', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (27, 'Película documental', 'info:ar-repo/semantics/película documental', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (28, 'Videograbación', 'info:ar-repo/semantics/videograbación', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (29, 'Conjunto de datos', 'info:ar-repo/semantics/conjunto de datos', 'info:eu-repo/semantics/other');
INSERT INTO public.coleccion VALUES (30, 'Proyecto de investigación', 'info:ar-repo/semantics/proyecto de investigación', 'info:eu-repo/semantics/other');


--
-- Data for Name: coleccion__version; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.coleccion__version VALUES (9, 1);
INSERT INTO public.coleccion__version VALUES (10, 1);
INSERT INTO public.coleccion__version VALUES (11, 1);
INSERT INTO public.coleccion__version VALUES (12, 1);
INSERT INTO public.coleccion__version VALUES (13, 1);
INSERT INTO public.coleccion__version VALUES (14, 1);
INSERT INTO public.coleccion__version VALUES (16, 1);
INSERT INTO public.coleccion__version VALUES (18, 1);
INSERT INTO public.coleccion__version VALUES (19, 1);
INSERT INTO public.coleccion__version VALUES (20, 1);
INSERT INTO public.coleccion__version VALUES (21, 1);
INSERT INTO public.coleccion__version VALUES (22, 1);
INSERT INTO public.coleccion__version VALUES (23, 1);
INSERT INTO public.coleccion__version VALUES (24, 1);
INSERT INTO public.coleccion__version VALUES (25, 1);
INSERT INTO public.coleccion__version VALUES (26, 1);
INSERT INTO public.coleccion__version VALUES (27, 1);
INSERT INTO public.coleccion__version VALUES (28, 1);
INSERT INTO public.coleccion__version VALUES (29, 1);
INSERT INTO public.coleccion__version VALUES (9, 2);
INSERT INTO public.coleccion__version VALUES (10, 2);
INSERT INTO public.coleccion__version VALUES (11, 2);
INSERT INTO public.coleccion__version VALUES (12, 2);
INSERT INTO public.coleccion__version VALUES (13, 2);
INSERT INTO public.coleccion__version VALUES (14, 2);
INSERT INTO public.coleccion__version VALUES (16, 2);
INSERT INTO public.coleccion__version VALUES (18, 2);
INSERT INTO public.coleccion__version VALUES (19, 2);
INSERT INTO public.coleccion__version VALUES (20, 2);
INSERT INTO public.coleccion__version VALUES (21, 2);
INSERT INTO public.coleccion__version VALUES (22, 2);
INSERT INTO public.coleccion__version VALUES (23, 2);
INSERT INTO public.coleccion__version VALUES (24, 2);
INSERT INTO public.coleccion__version VALUES (25, 2);
INSERT INTO public.coleccion__version VALUES (26, 2);
INSERT INTO public.coleccion__version VALUES (27, 2);
INSERT INTO public.coleccion__version VALUES (28, 2);
INSERT INTO public.coleccion__version VALUES (29, 2);
INSERT INTO public.coleccion__version VALUES (1, 3);
INSERT INTO public.coleccion__version VALUES (2, 3);
INSERT INTO public.coleccion__version VALUES (3, 3);
INSERT INTO public.coleccion__version VALUES (4, 3);
INSERT INTO public.coleccion__version VALUES (5, 3);
INSERT INTO public.coleccion__version VALUES (6, 3);
INSERT INTO public.coleccion__version VALUES (7, 3);
INSERT INTO public.coleccion__version VALUES (8, 3);
INSERT INTO public.coleccion__version VALUES (9, 3);
INSERT INTO public.coleccion__version VALUES (10, 3);
INSERT INTO public.coleccion__version VALUES (11, 3);
INSERT INTO public.coleccion__version VALUES (12, 3);
INSERT INTO public.coleccion__version VALUES (13, 3);
INSERT INTO public.coleccion__version VALUES (14, 3);
INSERT INTO public.coleccion__version VALUES (15, 3);
INSERT INTO public.coleccion__version VALUES (18, 3);
INSERT INTO public.coleccion__version VALUES (19, 3);
INSERT INTO public.coleccion__version VALUES (20, 3);
INSERT INTO public.coleccion__version VALUES (21, 3);
INSERT INTO public.coleccion__version VALUES (22, 3);
INSERT INTO public.coleccion__version VALUES (23, 3);
INSERT INTO public.coleccion__version VALUES (24, 3);
INSERT INTO public.coleccion__version VALUES (25, 3);
INSERT INTO public.coleccion__version VALUES (26, 3);
INSERT INTO public.coleccion__version VALUES (27, 3);
INSERT INTO public.coleccion__version VALUES (28, 3);
INSERT INTO public.coleccion__version VALUES (29, 3);
INSERT INTO public.coleccion__version VALUES (30, 3);
INSERT INTO public.coleccion__version VALUES (1, 4);
INSERT INTO public.coleccion__version VALUES (2, 4);
INSERT INTO public.coleccion__version VALUES (3, 4);
INSERT INTO public.coleccion__version VALUES (4, 4);
INSERT INTO public.coleccion__version VALUES (5, 4);
INSERT INTO public.coleccion__version VALUES (6, 4);
INSERT INTO public.coleccion__version VALUES (7, 4);
INSERT INTO public.coleccion__version VALUES (8, 4);
INSERT INTO public.coleccion__version VALUES (9, 4);
INSERT INTO public.coleccion__version VALUES (10, 4);
INSERT INTO public.coleccion__version VALUES (11, 4);
INSERT INTO public.coleccion__version VALUES (12, 4);
INSERT INTO public.coleccion__version VALUES (13, 4);
INSERT INTO public.coleccion__version VALUES (14, 4);
INSERT INTO public.coleccion__version VALUES (15, 4);
INSERT INTO public.coleccion__version VALUES (17, 4);
INSERT INTO public.coleccion__version VALUES (18, 4);
INSERT INTO public.coleccion__version VALUES (19, 4);
INSERT INTO public.coleccion__version VALUES (20, 4);
INSERT INTO public.coleccion__version VALUES (21, 4);
INSERT INTO public.coleccion__version VALUES (22, 4);
INSERT INTO public.coleccion__version VALUES (23, 4);
INSERT INTO public.coleccion__version VALUES (24, 4);
INSERT INTO public.coleccion__version VALUES (25, 4);
INSERT INTO public.coleccion__version VALUES (26, 4);
INSERT INTO public.coleccion__version VALUES (27, 4);
INSERT INTO public.coleccion__version VALUES (28, 4);
INSERT INTO public.coleccion__version VALUES (30, 4);
INSERT INTO public.coleccion__version VALUES (1, 5);
INSERT INTO public.coleccion__version VALUES (2, 5);
INSERT INTO public.coleccion__version VALUES (3, 5);
INSERT INTO public.coleccion__version VALUES (4, 5);
INSERT INTO public.coleccion__version VALUES (5, 5);
INSERT INTO public.coleccion__version VALUES (6, 5);
INSERT INTO public.coleccion__version VALUES (7, 5);
INSERT INTO public.coleccion__version VALUES (8, 5);
INSERT INTO public.coleccion__version VALUES (9, 5);
INSERT INTO public.coleccion__version VALUES (10, 5);
INSERT INTO public.coleccion__version VALUES (11, 5);
INSERT INTO public.coleccion__version VALUES (12, 5);
INSERT INTO public.coleccion__version VALUES (13, 5);
INSERT INTO public.coleccion__version VALUES (14, 5);
INSERT INTO public.coleccion__version VALUES (15, 5);
INSERT INTO public.coleccion__version VALUES (17, 5);
INSERT INTO public.coleccion__version VALUES (18, 5);
INSERT INTO public.coleccion__version VALUES (19, 5);
INSERT INTO public.coleccion__version VALUES (20, 5);
INSERT INTO public.coleccion__version VALUES (21, 5);
INSERT INTO public.coleccion__version VALUES (22, 5);
INSERT INTO public.coleccion__version VALUES (23, 5);
INSERT INTO public.coleccion__version VALUES (24, 5);
INSERT INTO public.coleccion__version VALUES (25, 5);
INSERT INTO public.coleccion__version VALUES (26, 5);
INSERT INTO public.coleccion__version VALUES (27, 5);
INSERT INTO public.coleccion__version VALUES (28, 5);


--
-- Name: coleccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.coleccion_seq', 31, false);


--
-- Data for Name: configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.configuracion VALUES (1, 'licencia', 'Queda un último paso: para permitir a DSpace reproducir, traducir y distribuir su envío a través del mundo, necesitamos su conformidad en los siguientes términos.
Conceda la licencia de distribución estándar seleccionando ''Conceder licencia'' y pulsando ''Completar envío''.

NOTE: PLACE YOUR OWN LICENSE HERE This sample license is provided for informational purposes only.
NON-EXCLUSIVE DISTRIBUTION LICENSE
By signing and submitting this license, you (the author(s) or copyright owner) grants to DSpace University (DSU) the non-exclusive right to reproduce, translate (as defined below), and/or distribute your submission (including the abstract) worldwide in print and electronic format and in any medium, including but not limited to audio or video.
You agree that DSU may, without changing the content, translate the submission to any medium or format for the purpose of preservation.
You also agree that DSU may keep more than one copy of this submission for purposes of security, back-up and preservation.
You represent that the submission is your original work, and that you have the right to grant the rights contained in this license. You also represent that your submission does not, to the best of your knowledge, infringe upon anyone''s copyright.
If the submission contains material for which you do not hold copyright, you represent that you have obtained the unrestricted permission of the copyright owner to grant DSU the rights required by this license, and that such third-party owned material is clearly identified and acknowledged within the text or content of the submission.
IF THE SUBMISSION IS BASED UPON WORK THAT HAS BEEN SPONSORED OR SUPPORTED BY AN AGENCY OR ORGANIZATION OTHER THAN DSU, YOU REPRESENT THAT YOU HAVE FULFILLED ANY RIGHT OF REVIEW OR OTHER OBLIGATIONS REQUIRED BY SUCH CONTRACT OR AGREEMENT.
DSU will clearly identify your name(s) as the author(s) or owner(s) of the submission, and will not make any alteration, other than as allowed by this license, to your submission.

Si tiene alguna duda sobre la licencia, por favor, contacte con el administrador del sistema.
                                ');


--
-- Name: configuracion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.configuracion_seq', 5, false);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.usuario VALUES (1, 'admin@admin.com', 'Super', 'Admin', '["ROLE_SUPER_ADMIN"]', '$argon2id$v=19$m=65536,t=4,p=1$M21sS1pzeUx6MFZIRG1UaA$FPySgyQSU3AeVXZEXEhHwmpXmyxs0gUunT7jXcuPYgM');


--
-- Name: usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_seq', 2, true);


--
-- Data for Name: version; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.version VALUES (1, 'Borrador', 'info:eu-repo/semantics/draft');
INSERT INTO public.version VALUES (2, 'Presentado', 'info:eu-repo/semantics/submittedVersion');
INSERT INTO public.version VALUES (3, 'Aceptado', 'info:eu-repo/semantics/acceptedVersion');
INSERT INTO public.version VALUES (4, 'Publicado', 'info:eu-repo/semantics/publishedVersion');
INSERT INTO public.version VALUES (5, 'Actualizado', 'info:eu-repo/semantics/updatedVersion');


--
-- Name: version_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.version_seq', 6, false);


--
-- Name: coleccion__version coleccion__version_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coleccion__version
    ADD CONSTRAINT coleccion__version_pkey PRIMARY KEY (id_coleccion, id_version);


--
-- Name: coleccion coleccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coleccion
    ADD CONSTRAINT coleccion_pkey PRIMARY KEY (id);


--
-- Name: configuracion configuracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuracion
    ADD CONSTRAINT configuracion_pkey PRIMARY KEY (id);


--
-- Name: version version_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.version
    ADD CONSTRAINT version_pkey PRIMARY KEY (id);


--
-- Name: coleccion__version_version_id_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX coleccion__version_version_id_fk ON public.coleccion__version USING btree (id_version);


--
-- Name: coleccion__version coleccion__version_coleccion_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coleccion__version
    ADD CONSTRAINT coleccion__version_coleccion_id_fk FOREIGN KEY (id_coleccion) REFERENCES public.coleccion(id);


--
-- Name: coleccion__version coleccion__version_version_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coleccion__version
    ADD CONSTRAINT coleccion__version_version_id_fk FOREIGN KEY (id_version) REFERENCES public.version(id);


--
-- PostgreSQL database dump complete
--

